//chapter 1 Where are you from ?

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

final FlutterTts flutterTts = FlutterTts();
speak(phrase) async {
  await flutterTts.speak(phrase);
}

class Chapter_4 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Chapter_4_State();
  }
}

class Chapter_4_State extends State<Chapter_4> {
  @override
  void initState() {
    speak('This is the simple expression about how to asking place location');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Chapter 4 : Direction Asking"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[DialogContainer()],
        ),
      ),
    );
  }
}

class DialogContainer extends StatelessWidget {
  final double buttonHeight = 15.00;
  final double boxHeight = 10.00;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              'Where is the bus terminal ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Where is the bus terminal ?')},
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              "It's next to the bank.",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak("It's next to the bank.")},
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              'How can we get to the bank ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('How can we get to the bank ?')},
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              "Go straight and turn left at the corner.",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () =>
                {speak('Go straight and turn left at the corner.')},
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              'Where the post office ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Where the post office ?')},
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: buttonHeight),
            child: Text(
              "It's opposite the police station",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () =>
                {speak("It's opposite the police station")},
          ),
        ],
      ),
    );
  }
}
